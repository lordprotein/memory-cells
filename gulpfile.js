var gulp  = require('gulp');
var babel = require('gulp-babel');
var less  = require('gulp-less');
var autoprefixer_css = require('gulp-autoprefixer');


var src = {
	js: {
		es5: 'src/js/es5',
		all: 'src/js/all scripts',
	},
	less: 'src/less',
	css: 'src/css',

}

function js_compile(done) {
	gulp.src(src.js.es5 + '/*.js')
	.pipe(babel({
		presets: ['@babel/env']
	}))
	.on('error', function (err) { console.log(err.message); })
	.pipe(gulp.dest(src.js.all));
	done();
}


function styles_compile(done) {
	gulp.src(src.less + '/styles.less')
	.pipe(less())
	.pipe(autoprefixer_css({
            browsersList: ['last 100 versions', 'ie 8', 'ie 9'],
            cascade: true
        }))
	.pipe(gulp.dest(src.css));
	done();
}



gulp.task('js_compile', js_compile);
gulp.task('styles_compile', styles_compile);

gulp.task('watch', function() {
	gulp.watch(src.js.es5 + '/**/*.js', gulp.parallel('js_compile'));
	gulp.watch(src.less + '/**/*.less', gulp.parallel('styles_compile'));
});

