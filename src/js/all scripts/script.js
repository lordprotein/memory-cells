"use strict";

var settings = {
  lvl: 0,
  // durationShowing: 3000,	
  addCellLvl: 6,
  //Count add new cells with new lvl
  tryingsCount: 3,
  //Count mistakes
  collectionCells: {},
  //Collection with classes div cell
  selectedCells: [],
  //Selected random cells (get index cells)
  countCells: 6,
  subLvl: 1,
  calcLvl: function calcLvl(level) //add new cells when we have lvl up
  {
    this.lvl += 1;

    if (this.lvl == 1 || this.lvl == 2 || this.lvl == 3) {
      this.countCells = this.addCellLvl;
    }

    if (!(this.lvl % 3)) {
      this.countCells = this.lvl * this.addCellLvl;
    }
  },
  restart: function restart() {
    this.selectedCells = [];
    this.lvl = 0;
    this.subLvl = 3;

    for (var i = 0; i < this.collectionCells.length; i++) {
      this.collectionCells[i].style.background = '';
    } // alert('Game over');


    console.log(this);
  },
  createDom: function createDom() {
    this.calcLvl();
    clearDomClassElem('ms-game-area');
    var cellElem = document.getElementsByClassName('ms-game-area')[0];

    for (var elemCell = 1; elemCell <= this.countCells; elemCell++) {
      var createCell = document.createElement('div');
      createCell.classList.add('ms-game-area__cell');
      cellElem.append(createCell);
    }
  }
};
var cellSettings = {
  activeColor: 'red' //-------------------------------------------------------------

};

function eventListeners() {
  settings.createDom(); //EVENT BUTTON *START* ---------------

  var buttonStart = document.getElementsByClassName('button-start')[0];
  buttonStart.addEventListener('click', function () {
    settings.restart();
    generateSelectCells();
  }); //END EVENT BUTTON *START* -----------
}

function generateSelectCells() {
  settings.collectionCells = document.getElementsByClassName('ms-game-area__cell'); //add all cells how array in prop

  for (var i = 0; i < settings.subLvl; i++) {
    var randomCell = showedRandCells(1, settings.collectionCells.length);
    var arrSelectedCells = settings.selectedCells;

    if (arrSelectedCells.includes(randomCell)) {
      while (arrSelectedCells.includes(randomCell)) {
        randomCell = showedRandCells(1, settings.collectionCells.length);
      }
    }

    settings.selectedCells.push(randomCell); //insert random selected number

    settings.collectionCells[randomCell - 1].style.background = cellSettings.activeColor;
  }

  console.log(settings.selectedCells);
  clickCellEvent();
} //EVENT *CLICK ON CELL*-------	


function clickCellEvent() {
  var collectionCells = settings.collectionCells;
  var selectedCells = settings.selectedCells;
  var tryingsCounter = counter();
  var tryings;

  var _loop = function _loop(i) {
    collectionCells[i].addEventListener('click', function () {
      if (selectedCells.length) {
        //Check have array elements
        for (var num = 0; num < selectedCells.length; num++) {
          // console.log(selectedCells[num] == (i + 1));
          if (selectedCells[num] == i + 1) //True - click on selected cell
            {
              console.log(settings.selectedCells);
              console.log('Click on:' + (i + 1));
              console.log('In array: ' + num);
              trueClick(num, i);
              break;
            } else //False - click not a selected cell
            {// tryings = tryingsCounter();
              // let click = falseClick(tryings);
              // if(click) break;
              // alert(3);
              // break;
            }
        }
      }
    });
  };

  for (var i = 0; i < collectionCells.length; i++) {
    _loop(i);
  }
} //END EVENT *CLICK ON CELL*-------	


function trueClick(posArray, posCollection) {
  var collectionCells = settings.collectionCells;
  var selectedCells = settings.selectedCells;
  collectionCells[posCollection].style.background = 'blue';
  selectedCells.splice(posArray, 1); // console.log('Deleted... ' + (index+1) + '///Length: ' + settings.selectedCells.length);

  console.log('true click: array with del: ' + settings.selectedCells);

  if (!settings.selectedCells.length) {
    console.log('win');
    settings.subLvl++;
    settings.createDom();
    generateSelectCells();
  }
}

function falseClick(tryings) {
  // if(tryings == settings.tryingsCount)
  // {
  console.log('game over');
  settings.restart();
  return true; // }
} // function randomer()
// {
// 	let min = settings.countCells / 6;
// 	let max = settings.countCells / 2;
// 	// let randMax = Math.ceil(Math.random() * (max - (min+2)) + min);
// 	// return min + ' ' + max + ' ' + randMax;
// }


function counter(restart) {
  //generate lvl +1
  var begin = 0;

  function count() {
    begin++;
    return begin;
  }

  return count;
}

function showedRandCells(min, max) {
  var randNum = Math.ceil(Math.random() * (max - min) + min);
  return randNum;
}

function clearDomClassElem() {
  var nameClass = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var posArray = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var cells = document.getElementsByClassName(nameClass);
  var cellElem = cells[posArray];
  cellElem.innerHTML = '';
}

eventListeners();